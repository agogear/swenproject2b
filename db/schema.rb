# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150421105845) do

  create_table "daily_readings", force: :cascade do |t|
    t.datetime "date"
    t.integer  "location_id"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
  end

  create_table "locations", force: :cascade do |t|
    t.string   "name"
    t.float    "latitude"
    t.float    "longitude"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "rainfall_readings", force: :cascade do |t|
    t.float    "ammount"
    t.string   "unit"
    t.integer  "reading_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "readings", force: :cascade do |t|
    t.datetime "time"
    t.integer  "daily_reading_id"
    t.string   "source"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "temperature_readings", force: :cascade do |t|
    t.float    "current_temperature"
    t.float    "dew_point"
    t.integer  "reading_id"
    t.datetime "created_at",          null: false
    t.datetime "updated_at",          null: false
  end

  create_table "wind_readings", force: :cascade do |t|
    t.float    "wind_speed"
    t.string   "wind_direction"
    t.integer  "reading_id"
    t.datetime "created_at",     null: false
    t.datetime "updated_at",     null: false
  end

end
