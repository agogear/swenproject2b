class CreateReadings < ActiveRecord::Migration
  def change
    create_table :readings do |t|
      t.datetime :time
      t.integer :daily_reading_id
      t.string :source

      t.timestamps null: false
    end
  end
end
