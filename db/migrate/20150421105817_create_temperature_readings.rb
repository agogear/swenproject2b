class CreateTemperatureReadings < ActiveRecord::Migration
  def change
    create_table :temperature_readings do |t|
      t.float :current_temperature
      t.float :dew_point
      t.integer :reading_id

      t.timestamps null: false
    end
  end
end
