class CreateWindReadings < ActiveRecord::Migration
  def change
    create_table :wind_readings do |t|
      t.float :wind_speed
      t.string :wind_direction
      t.integer :reading_id

      t.timestamps null: false
    end
  end
end
