class CreateRainfallReadings < ActiveRecord::Migration
  def change
    create_table :rainfall_readings do |t|
      t.float :ammount
      t.string :unit
      t.integer :reading_id

      t.timestamps null: false
    end
  end
end
