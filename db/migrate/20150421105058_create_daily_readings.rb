class CreateDailyReadings < ActiveRecord::Migration
  def change
    create_table :daily_readings do |t|
      t.datetime :date
      t.integer :location_id

      t.timestamps null: false
    end
  end
end
