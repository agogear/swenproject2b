
locations = [
		{name: "Melbourne (Olympic Park)", latitude: -37.8267, longitude: 144.9811},
		{name: "Melbourne Airport", latitude: -37.668846, longitude: 144.841059},
		{name: "Avalon", latitude: -38.023527, longitude: 144.476786},
		{name: "Bundora", latitude: -37.697970, longitude: 145.058281},
		{name: "Cerberus", latitude: -37.966794, longitude: 145.007880},
		{name: "Cranbourne", latitude:-38.109008, longitude: 145.284404}, 
		{name: "Coldstream", latitude: -37.726328, longitude: 145.381216},
		{name: "Essedon", latitude: -37.755530, longitude: 144.917315},
		{name: "Fawkner", latitude: -37.715084, longitude: 144.962360},
		{name: "Ferny Creek", latitude: -37.881527, longitude: 145.341638},
		{name: "Frankston", latitude: -38.142284, longitude: 145.121339},
		{name: "Geelong Race Course", latitude: -38.175739, longitude: 144.371907},
		{name: "Laverton", latitude: -37.862332, longitude: 144.769087},
		{name: "Moorabbin Airport", latitude: -37.974605, longitude: 145.095433},
		{name: "Point Wilson", latitude: -38.019150, longitude: 144.499912},
		{name: "Rhyll", latitude: -38.464174, longitude: 145.304991},
		{name: "Scoresby", latitude: -37.898745, longitude: 145.233098},
		{name: "Sheoaks", latitude: -37.901368, longitude: 144.134217},
		{name: "South Channel Island", latitude: -38.306423, longitude: 144.801782},
		{name: "St Kilda Harbour RMYS",latitude: -37.863550, longitude: 144.971709},
		{name: "Viewbank", latitude: -37.732748, longitude: 145.092150}
	]

locations.each do |hash|
	Location.create(hash)
end