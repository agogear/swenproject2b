class Reading < ActiveRecord::Base
	belongs_to :daily_reading
	has_one :rainfall_reading
	has_one :temperature_reading
	has_one :wind_reading	
end
