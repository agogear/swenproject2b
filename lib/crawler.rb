require 'nokogiri'
require 'open-uri'
require 'json'
require 'forecast_io'
require 'geocoder'

# This class contains the methods to parse website and add data to the database,
# I opted to use some not so nice constructs a.k.a huge lists with lots of values
# because it was not data enough to justify creating a data model just for that.
class Crawler

	# Proc to associate keys and values, very useful to generate parameters
	@@to_hash = Proc.new { |keys,values|
		hash = {}
		[keys,values].transpose.each do |key,value|
			hash[key] = value
		end
		hash
	}

	@@api_key = '2b1ccb118963d19acec6148a252d9448'

	# Given a list of lists where for each element i:
	# - list[i][0] = parameters for finding an instance of the Location model.
	# - list[i][1] = parameters for creating an instance of the DailyReading model.
	# - list[i][2] = parameters for creating an instance of the Reading model.
	# - list[i][3..5] = parameters for creating an instance of RainfallReading
	#   TemperatureReading and WindReading models.
	# Create those models if they don't already exist.
	def self.create_data data

		data.each do |row|

			# handles the creation of the * if needed

			l = Location.find_by(row[0])

			if l == nil
				next
			end

			row[1][:location_id] = l.id
			dr = DailyReading.find_or_create_by(row[1])

			puts 'teste'
			row[2][:daily_reading_id] = dr.id
			r = Reading.find_or_create_by(row[2])

			row[3][:reading_id] = r.id
			row[4][:reading_id] = r.id
			row[5][:reading_id] = r.id
			rr = RainfallReading.find_or_create_by(row[3])
			tr = TemperatureReading.find_or_create_by(row[4])
			wr = WindReading.find_or_create_by(row[5])

			# save references
			dr.save
			r.save
			rr.save
			tr.save
			wr.save
		end	
	end


	def self.nil_or_value string
		if string == '-'
			return nil
		else
			return string
		end
	end

	# Gets the data from => http://www.bom.gov.au/vic/observations/melbourne.shtml
	def self.get_bom

		# Attempts pen the HTML link with Nokogiri
		begin
			url = 'http://www.bom.gov.au/vic/observations/melbourne.shtml'
			doc = Nokogiri::HTML(open(url))
		rescue
			puts "Could not access http://www.bom.gov.au/ at " + DateTime.now.to_s
			return
		end

		# Gets the main table in doc
		doc = doc.css('tbody tr')

		# Iterates through each row of the table
		details = doc.collect do |row|

			# gets the current date string
			date_string = row.at_css('td[2]').text

			# gets the current date
			date_complex = (DateTime.now.strftime("%Y/%m/") + date_string).to_datetime

			# get a variable only with year month day
			date_simple = (DateTime.now.strftime("%Y/%m/") + date_string.split("/")[0]).to_datetime

			# get the source
			source = 'http://www.bom.gov.au/vic/observations/melbourne.shtml'

			location_name_value = nil_or_value row.at_css('th a').text

			rainfall_ammount_value = nil_or_value row.at_css('td[14]').text

			current_temperature_value = nil_or_value row.at_css('td[3]').text

			current_dew_point_value = nil_or_value row.at_css('td[5]').text

			wind_direction_value = nil_or_value row.at_css('td[8]').text

			wind_speed_value = nil_or_value row.at_css('td[9]').text

			# CREATES HASH location
			location = @@to_hash.call [:name], [location_name_value]

			# CREATES HASH date
			date = @@to_hash.call [:date], [date_simple]

			# CREATES HASH reading
			reading = @@to_hash.call [:source,:time], [source, date_complex]

			# CREATES HASH rainfall
			rainfall = @@to_hash.call [:ammount, :unit], [ rainfall_ammount_value, "mm since 9am"]

			# CREATES HASH temperature
			temperature = @@to_hash.call [:current_temperature,:dew_point], [current_temperature_value.to_f, current_dew_point_value.to_f]

			# CREATES HASH wind
			wind = @@to_hash.call [:wind_direction, :wind_speed], [wind_direction_value, wind_speed_value.to_f]

			# adds everything to a bad-ass list
			[location,date,reading,rainfall,temperature,wind]
		end

		create_data details

		puts "Updated with http://www.bom.gov.au at " + DateTime.now.to_s
	end

	# Gets the data from => forecast.io
	def self.get_dsf

		ForecastIO.api_key = @@api_key

			details =  Location.all.collect do |place|

				#accest forecast.io
				forecast = ForecastIO.forecast(place.latitude, place.longitude, params: { units: 'ca' })[:currently]

				# get wind speed
				wind_speed = forecast[:windSpeed]

				# get wind direction
				if wind_speed == 0
					wind_direction = 'CALM'
				else
					wind_direction = Geocoder::Calculations.compass_point(forecast[:windBearing])
				end
					
				# get time
				time = Time.at(forecast[:time]).to_s.to_datetime

				# CREATES HASH location
				location = @@to_hash.call [:name], [place.name] 

				# CREATES HASH date
				date =  @@to_hash.call [:date], [time.strftime("%Y/%m/%d").to_datetime]

				# CREATES HASH reading
				reading = @@to_hash.call [:source, :time], ['forecast.io', time]

				# CREATE HASH rainfall
				rainfall = @@to_hash.call [:ammount, :unit], [forecast[:precipIntensity], "mm per Hour (expected)"]

				# CREATES HASH temp
				temperature	= @@to_hash.call [:current_temperature, :dew_point], [forecast[:temperature],forecast[:dewPoint]]
			
				# CREATE HASH WIND
				wind = @@to_hash.call [:wind_direction, :wind_speed], [wind_direction,wind_speed]

				# adds everything to a bad-ass list
				[location,date,reading,rainfall,temperature,wind]
			end

		create_data details

		puts "Updated with forecast.io at " + DateTime.now.to_s
	end
end