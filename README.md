In the terminal run:

  bundle install
  rake db:create
  rake db:migrate
  rake db:seed
  whenever --update-crontab 
  rails s

== Some notes on the project

We don't add any new locations in the crawling process. The locations are all initialized in the database seeds. If you need to add any locations just add the names and coordinates there.