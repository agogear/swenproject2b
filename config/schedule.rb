set :output, "#{path}/log/cron_log.log"

job_type :script_runner, "cd :path; rails runner :task :output"

every 5.minute do 
	script_runner "#{path}/lib/tasks/get_bom.rb"
end

every 24.minute do 
	script_runner "#{path}/lib/tasks/get_forecast_io.rb"
end